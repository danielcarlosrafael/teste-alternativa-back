﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using webApiProjeto.Models;
using webApiProjeto.Data;

namespace webApiProjeto.Controllers
{
    [Route("api/categorias")]
    public class CategoriaController : Controller
    {
        private ProdutoContext db = new ProdutoContext();

        [Produces("application/json")]
        [HttpGet("findall")]
        public async Task<IActionResult> findAll()
        {
            try
            {
                var categorias = db.Categoria.ToList();
                return Ok(categorias);
            }
            catch
            {
                return BadRequest();
            }
        }

        [Produces("application/json")]
        [HttpGet("find/{id}")]
        public async Task<IActionResult> find(int id)
        {
            try
            {
                var categorias = db.Categoria.Find(id);
                return Ok(categorias);
            }
            catch
            {
                return BadRequest();
            }
        }

        [Produces("application/json")]
        [Consumes("application/json")]
        [HttpPost("create")]
        public async Task<IActionResult> create([FromBody]Categoria categoria)
        {
            try
            {
                db.Categoria.Add(categoria);
                db.SaveChanges();
                return Ok(categoria);
            }
            catch
            {
                return BadRequest();
            }
        }

        [Produces("application/json")]
        [Consumes("application/json")]
        [HttpPut("update")]
        public async Task<IActionResult> update([FromBody] Categoria categoria)
        {
            try
            {
                db.Entry(categoria).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                db.SaveChanges();
                return Ok(categoria);
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpDelete("delete/{id}")]
        public async Task<IActionResult> delete(int id)
        {
            try
            {
                db.Categoria.Remove(db.Categoria.Find(id));
                db.SaveChanges();
                return Ok();
            }
            catch
            {
                return BadRequest();
            }
        }
    }
}
