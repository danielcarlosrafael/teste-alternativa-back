﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using webApiProjeto.Models;
using webApiProjeto.Data;

namespace webApiProjeto.Controllers
{
    [Route("api/produtos")]
    public class ProdutoController : Controller
    {
        private ProdutoContext db = new ProdutoContext();

        [Produces("application/json")]
        [HttpGet("findall")]
        public async Task<IActionResult> findAll()
        {
            try
            {
                var produtos = db.Produto.ToList();
                return Ok(produtos);
            }
            catch
            {
                return BadRequest();
            }
        }

        [Produces("application/json")]
        [HttpGet("find/{id}")]
        public async Task<IActionResult> find(int id)
        {
            try
            {
                var produtos = db.Produto.Find(id);
                return Ok(produtos);
            }
            catch
            {
                return BadRequest();
            }
        }

        [Produces("application/json")]
        [Consumes("application/json")]
        [HttpPost("create")]
        public async Task<IActionResult> create([FromBody] Produto produto)
        {
            try
            {
                db.Produto.Add(produto);
                db.SaveChanges();
                return Ok(produto);
            }
            catch
            {
                return BadRequest();
            }
        }

        [Produces("application/json")]
        [Consumes("application/json")]
        [HttpPut("update")]
        public async Task<IActionResult> update([FromBody] Produto produto)
        {
            try
            {
                db.Entry(produto).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                db.SaveChanges();
                return Ok(produto);
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpDelete("delete/{id}")]
        public async Task<IActionResult> delete(int id)
        {
            try
            {
                db.Produto.Remove(db.Produto.Find(id));
                return Ok();
            }
            catch
            {
                return BadRequest();
            }
        }
    }
}
