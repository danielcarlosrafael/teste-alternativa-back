﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webApiProjeto.Models
{
    public class Produto
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public float value { get; set; }
        public string brand { get; set; }
        public Categoria category_id { get; set; }
    }
}
